package main

import (
	"bufio"
	"errors"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type Option int

const (
	ROCK     Option = 1
	PAPER    Option = 2
	SCISSORS Option = 3
)

type Player struct {
	name   string
	option Option
}

func main() {
	p1 := Player{name: "Player 1"}
	p2 := Player{name: "Player 2"}

	printOptions(p1.name)
	o1, _ := readOption()
	p1.option = o1

	printOptions(p2.name)
	o2, _ := readOption()
	p2.option = o2

	getWinner(p1, p2)
}

func getWinner(p1 Player, p2 Player) {
	if p1.option == p2.option {
		fmt.Println("Draw")
		return
	}

	if p1.option == ROCK || p2.option == ROCK {
		if p1.option == ROCK && p2.option == SCISSORS {
			fmt.Println("Player 1 Winner")
			return
		} else if p2.option == ROCK && p1.option == SCISSORS {
			fmt.Println("Player 2 Winner")
			return
		}
	}

	if p1.option == SCISSORS || p2.option == SCISSORS {
		if p1.option == SCISSORS && p2.option == PAPER {
			fmt.Println("Player 1 Winner")
			return
		} else if p2.option == SCISSORS && p1.option == PAPER {
			fmt.Println("Player 2 Winner")
			return
		}
	}

	if p1.option == PAPER || p2.option == PAPER {
		if p1.option == PAPER && p2.option == ROCK {
			fmt.Println("Player 1 Winner")
			return
		} else if p2.option == PAPER && p1.option == ROCK {
			fmt.Println("Player 2 Winner")
			return
		}
	}
}

func printOptions(p string) {
	fmt.Println()
	fmt.Println(p)
	fmt.Println("Choose the options:")
	fmt.Println("1. Rock")
	fmt.Println("2. Paper")
	fmt.Println("3. Scissors")
}

func readOption() (Option, error) {
	r := bufio.NewReader(os.Stdin)
	s := readNextIntLine(r)

	switch s {
	case 1:
		return ROCK, nil
	case 2:
		return PAPER, nil
	case 3:
		return SCISSORS, nil
	default:
		return 0, errors.New("Wrong value.")
	}

}

func readNextIntLine(r *bufio.Reader) int {
	line, _ := r.ReadString('\n')
	x, _ := strconv.Atoi(strings.Trim(line, "\n"))
	return x
}
